# LDAP theory
Here you will find both the theory and the structures of the files for .ldif, as well as the different most important options of the main commands.
## Index
1. [What is LDAP?](#What-is-ldap?)
2. [LDAP Elements](#lldap-elements)
3. [ldapsearch - command and options](#ldapsearch-command-and-options)
3. [ldapadd - basic file structure](#ldapadd-basic-file-structure)
4. [ldapadd - command and options](#ldapadd-command-and-options)
5. [ldapmodify - basic file structure](#ldapmodify-basic-file-structure)
6. [ldapmodify - command and options](#ldapmodify-command-and-options)

## What is LDAP?
LDAP stands for Lightweight Directory Access Protocol.
We can understand the LDAP system as a database, although its structure and operation are very different from a conventional one. The LDAP structure is **optimized for reading**, but the writing functions are slow.

In LDAP, information is **structured in the form of a tree**. This tree is called the Directory Information Tree (DIT).

![LDAP Tree Outline](Imatges/EsquemaArbreLDAP.png)

| | |
| :-- | :--: |
| Traditionally, this structure has reflected geographical and organizational boundaries. At the top of the tree are represented countries, and below, states, organizations, organizational units, people, computers, printers, documents, or other concepts. | ! [LDAP Tree Outline] (Imatges/EsquemaTradicionalLDAP.png) |
| The directory tree can also be structured based on Internet Domain Names (DNS). This practice is becoming more widespread, as it allows you to find an LDAP server by performing a DNS query. This type of representation is the most common today when it comes to implementing directory services to manage computer domains. | ![LDAP Tree Schema](Imatges/EsquemaActualLDAP.png) |

## Elements of the LDAP
- **Root**: is the equivalent of what we know as **domain**. Your appointment will be of the type example.com. However, it will be separated as follows in LDAP formats: dc=example,dc=com
- **Organizational Units**: As in windows server, we use the OUs to organize the information. The difference, this time the system if you use the OU structure for the **tree organization** (BDD).
- **Objects**: These are the **end *tips* or *leaves* of the tree**, although they may have subtypes. They contain the information of each of the elements. They are similar to objects used in programming. They contain the characteristic attributes of the object.
- **Entry**: Each node (*leaf*) in the tree is called an entry. An entry is a **collection of attributes identified by a distinguished name** (DN). This DN is unique in the directory and uniquely refers to the entry (in the same way that an identifier uniquely characterizes a record in a relational database).
This name represents the **path from the position of the entry to the root** of the tree (base, according to the LDAP nomenclature). Because a directory is supposed to be used to store the objects of a particular organization, the basis will be the location of that organization. Thus, the database becomes the suffix of all directory entries.
- **Attributes**: **Entries consist of a set of attributes**. Each attribute of an entry has a type and a value (SINGLE-VALUE) or more (MULTI-VALUE, which is the default behavior). The attribute type is associated with the allowed values ​​and their behavior when making comparisons, sorting, or working with substrings.
Types are usually mnemonic text strings, such as cn to refer to common name, sn a surname, or mail a email address.
The syntax of the values ​​will depend on the type of the attribute, for example, the cn attribute could contain the value “Marc Freixas”, the mail attribute could contain a value like “mfreixas@example.com” and a jpegPhoto attribute would contain a image in binary format.
  
  - [ ] *You can find examples and references in the [ldapadd - basic file structure](#ldapadd-basic-file-structure) section.*

## ldapsearch - command and options
 ```
 ldapsearch -x -LLL -b "dc = example, dc = com" -H "ldap: // IP" '(attribute = filter)'
 ```
- 1. ldapsearch is the command to perform searches on the LDAP server.
- 2. The return will be by STANDARD_OUTPUT in plain text formatted in ldif.
- 3. The options:
```
-x Indicates that it will use simple identification
-LLL Formats the content by deleting commented information.
The 1ª L forces the format to ldifV1.
The 2ª L deletes comments.
3ª disables version printing.
-b Refers to the database, root or point where the search will be performed.
-H Used to make the request to a remote machine. The ldap protocol must be specified.
'(attribute = filter)' Filters can be performed by specific object types or attributes. For example '(objectClass=posixGroup)'.
```
*For more references you can consult the [web](https://linux.die.net/man/1/ldapsearch) or the man in the terminal.*

## ldapadd - basic file structure
### Organizational Unit
- dn: ou = **NAME**, *superior dn*
- objectClass: organizationalUnit
- objectClass: top
- ou: **NAME**

-Example
 ```
 dn: ou=land,ou=field,dc=example,dc=com
 objectClass: organizationalUnit
 objectClass: top
 ou: land
 ```

### Group
- dn: cn = **NAME**, *superior dn*
- gidNumber: 500
- objectClass: posixGroup
- objectClass: top
- cn: **NAME**
- memberUid: user

    - [ ] gidNumber is a unique identifier in the system. They start at 500.

    - [ ] memberUid can be more than one. Indicates that the user is part of the group.

-Example
 ```
 dn: cn=vegetables,ou=field,dc=example,dc=com
 gidNumber: 500
 objectClass: posixGroup
 objectClass: top
 cn: vegetables
 memberUid: potato
 memberUid: ctomato
 ```

### User
- dn: cn = **NAME AND SURNAME**, *superior dn*
- givenName: **NAME**
- gidNumber: 500
- homeDirectory: /home/users/**uid**
- sn: **SURNAME**
- loginShell: /bin/sh
- objectClass: inetOrgPerson
- objectClass: posixAccount
- objectClass: top
- uidNumber: 1000
- uid: **NAMESURNAME**

    - [ ] gidNumber is the main user group. Must be a pre-existing group.

    - [ ] homeDirectory is the home directory on the user's system.

    - [ ] loginShell is the default terminal for the user. They can be modified.

    - [ ] uidNumber is a unique identifier in the system. They start at 1000.

    - [ ] uid corresponds to the user ID in the login system. It is usually created with the initial of the name and surname. It must also be unique.

 ```
 dn: cn=cherry tomato,ou=land,ou=field,dc=example,dc=com
 givenName: cherry
 gidNumber: 500
 homeDirectory: /home/users/ctomato
 sn: tomato
 loginShell: / bin / sh
 objectClass: inetOrgPerson
 objectClass: posixAccount
 objectClass: top
 uidNumber: 1000
 uid: ctomato
 ```

## ldapadd - command and options
 ```
 ldapadd -c -x -D "cn=admin,dc=example,dc=com" -w "password" -f file.ldif
 ```
- 1. ldapadd is the command to import into the LDAP server, for both creation and modification.
- 2. The files to be imported must be in .ldif format.
     - 2.1 obviously have to maintain the ldif (LDAP) format internally.
- 3. The options:
```
-x Indicates that it will use simple identification
-D Indicates that a bind (login) will be performed with a user. How we will make an amount must be a user in administrator permissions.
-w It is the password.
-f This is the file to import.
-c Activates continuous operation mode. Report bugs but keep running.
```

## ldapmodify - basic file structure
- 1. Target
- 2. Changetype
- 3. What to do and attribute to modify
- 4. New attribute value

- ### Modify an attribute
```
dn: cn=cherry tomato,ou=land,ou=field,dc=example,dc=com
changetype: modify
replace: gidNumber
gidNumber: 505
```

- ### Add an attribute
```
dn: cn=vegetables,ou=field,dc=example,dc=com
changetype: modify
add: memberUid
memberUid: cabbage
```

- ### Delete an attribute
```
dn: cn=vegetables,ou=field,dc=example,dc=com
changetype: modify
delete: memberUid
memberUid: cabbage
```

- ### Delete an object completely
```
dn: cn=cherry tomato,ou=land,ou=field,dc=example,dc=com
changetype: delete
```

## ldapmodify - command and options
 ```
 ldapmodify -c -x -D "cn=admin,dc=example,dc=com" -w "password" -f file.ldif
 ```
- 1. ldapmodify is the command to make modifications to the LDAP server, both for creation and modification.
- 2. The files to be imported must be in .ldif format.
     - 2.1 obviously have to maintain the ldif (LDAP) format internally.
- 3. The options:
```
            -x Indicates that it will use simple identification.
            -D Indicates that a bind (login) will be performed with a user. How we will make an amount must be a user in administrator permissions.
            -w It is the password.
            -f This is the file to import.
            -c Activates continuous operation mode. Report bugs but keep running.
```
